(* ::Package:: *)

(* ::Title:: *)
(*ListGridPlot*)


(* ::Text:: *)
(*Rhys Povey <rhyspovey.com>*)


(* ::Section:: *)
(*Front End*)


BeginPackage["ListGridPlot`"];


ListGridPlot::usage="ListGridPlot[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] uses an ArrayPlot if the data is a regular complete grid, otherwise employs Graphics.";


ListGridPlot::input="Input must be of the form {{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}.";


ListGridGraphicsPlot::usage="ListGridPlot[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] draws rectangles at {\!\(\*SubscriptBox[\(x\), \(i\)]\),\!\(\*SubscriptBox[\(y\), \(i\)]\)} with coloring based on \!\(\*SubscriptBox[\(z\), \(i\)]\) using Graphics.";


ListGridSparseArrayPlot::usage="ListGridSparseArrayPlot[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] uses SparseArray and ArrayPlot, faster than Graphics, but currently does not have axes renaming implemented.";


ListGridArrayPlot::usage="ListGridArrayPlot[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] for data in a complete reguar grid uses ArrayPlot. Has option \"NumberFrameTicks\" to customize FrameTicks.";


ListGridArrayPlot::input="Input does not form a complete regular grid.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Subsection:: *)
(*Graphics approach*)


RegularGridSize[data_]:={
Min[Abs/@Differences[Union[data[[All,1]]]]],
Min[Abs/@Differences[Union[data[[All,2]]]]]
};


ListGridGraphicsPlot[data_,options:OptionsPattern[]]:=Module[
{datarange,valuerange,gridsize,rawcolourfunc,colourfunc,graphic},
gridsize=RegularGridSize[data];
datarange={MinMax[data[[All,1]]],MinMax[data[[All,2]]]};
valuerange=MinMax[data[[All,3]]];

rawcolourfunc=If[StringQ[OptionValue[ColorFunction]],ColorData[OptionValue[ColorFunction]],OptionValue[ColorFunction]];
colourfunc=If[OptionValue[ColorFunctionScaling],rawcolourfunc[Rescale[#,valuerange]]&,rawcolourfunc];

(* graphic=Graphics[{If[OptionValue[Mesh],EdgeForm[OptionValue[MeshStyle]],Nothing]}~Join~Flatten[{colourfunc[#[[3]]],Rectangle[#[[1;;2]]-gridsize/2,#[[1;;2]]+gridsize/2]}&/@data,1],(#->OptionValue[#])&/@Keys[Options[Graphics]]]; *)
graphic=Graphics[
{If[OptionValue[Mesh],EdgeForm[OptionValue[MeshStyle]],Nothing]}~Join~Flatten[{colourfunc[#[[3]]],Rectangle[#[[1;;2]]-gridsize/2,#[[1;;2]]+gridsize/2]}&/@data,1],
FilterRules[{options},Select[Options[Graphics],FreeQ[{AspectRatio,Frame},Keys[#]]&]]~Join~
{Frame->OptionValue[Frame],AspectRatio->OptionValue[AspectRatio]}
];

Switch[OptionValue[PlotLegends],
None,graphic,
Automatic,Row[{graphic,BarLegend[{colourfunc,valuerange},LegendMarkerSize->OptionValue[ImageSize]]}],
_,Row[{graphic,OptionValue[PlotLegends]}]
]
]/;If[And[Length[Dimensions[data]]==2,Dimensions[data][[2]]==3],True,Message[ListGridPlot::input];False];


Options[ListGridGraphicsPlot]=Select[Options[Graphics],FreeQ[{AspectRatio,Frame},Keys[#]]&]~Join~{
ColorFunction->(Blend[{White,Black},#]&),
ColorFunctionScaling->True,
Mesh->False,
MeshStyle->GrayLevel[GoldenRatio-1],
PlotLegends->Automatic,
Frame->True,
AspectRatio->1
};


(* ::Subsection:: *)
(*SparseArray approach*)


(* ::Text:: *)
(*Doesn't have tick renaming implemented.*)


ListGridSparseArrayPlot[data_,options:OptionsPattern[ArrayPlot]]:=Module[{datarange,gridsize,sparsearray},
gridsize=RegularGridSize[data];
datarange={MinMax[data[[All,1]]],MinMax[data[[All,2]]]};
sparsearray=SparseArray[(Round[{(datarange[[2,2]]-#[[2]])/gridsize[[2]]+1,(#[[1]]-datarange[[1,1]])/gridsize[[1]]+1},1]->#[[3]])&/@data];

(* Need to be selective with option passing *)
ArrayPlot[sparsearray,
FilterRules[{options},Select[Options[ArrayPlot],FreeQ[{FrameLabel},Keys[#]]&]]~Join~
{FrameLabel->Reverse[OptionValue[FrameLabel]]}
]
]/;If[And[Length[Dimensions[data]]==2,Dimensions[data][[2]]==3],True,Message[ListGridPlot::input];False];


Options[ListGridSparseArrayPlot]=Select[Options[ArrayPlot],FreeQ[{FrameLabel},Keys[#]]&]~Join~{
FrameLabel->{None,None}
};


(* ::Subsection:: *)
(*Complete Grid Array Plot*)


(* ::Text:: *)
(*If the data makes up a complete grid, conversion to a pixel array and ArrayPlot is much faster.*)


CompleteGridCheck::usage="CompleteGridCheck[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] checks if {\!\(\*SubscriptBox[\(x\), \(i\)]\),\!\(\*SubscriptBox[\(y\), \(i\)]\)} form a complete regular grid.";


CompleteGridCheck[data_]:=And[
Length[Union[GatherBy[data,#[[1]]&][[All,All,2]]]]==1,
Length[Union[GatherBy[data,#[[2]]&][[All,All,1]]]]==1
]/;If[And[Length[Dimensions[data]]==2,Dimensions[data][[2]]>=2],True,Message[ListGridPlot::input];False];


ConvertGridPointsToArray::usage="ConvertGridPointsToArray[{{\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(y\), \(1\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\)},\[Ellipsis]}] converts data to array format.";


ConvertGridPointsToArray[data_]:=Reverse[GatherBy[Sort[data],#[[2]]&]][[All,All,3]];


ListGridArrayPlot[data_,options:OptionsPattern[]]:=Module[{
horizvalues=Union[data[[All,1]]],
vertvalues=Reverse[Union[data[[All,2]]]],
nhoriz,nvert,nhorizticks,nvertticks,
frameticks
},
nhorizticks=OptionValue["NumberFrameTicks"][[1]];
nvertticks=OptionValue["NumberFrameTicks"][[2]];
nhoriz=Length[horizvalues];
nvert=Length[vertvalues];
frameticks={
{#,vertvalues[[#]]}&/@(Round/@Range[1,nvert,(nvert-1)/(nvertticks-1)]),
{#,horizvalues[[#]]}&/@(Round/@Range[1,nhoriz,(nhoriz-1)/(nhorizticks-1)])
};

(* Need to be selective with option passing *)
ArrayPlot[
ConvertGridPointsToArray[data],
FilterRules[{options},Select[Options[ArrayPlot],FreeQ[{FrameTicks,FrameLabel},Keys[#]]&]]~Join~
{FrameTicks->frameticks,FrameLabel->Reverse[OptionValue[FrameLabel]]}
]

]/;If[CompleteGridCheck[data],True,Message[ListGridArrayPlot::input];False];


Options[ListGridArrayPlot]=Select[Options[ArrayPlot],FreeQ[{FrameTicks,FrameLabel},Keys[#]]&]~Join~{
"NumberFrameTicks"->{5,5},
FrameLabel->{None,None}
};


(* ::Subsection:: *)
(*Master*)


ListGridPlot[data_,options:OptionsPattern[]]:=If[CompleteGridCheck[data],
ListGridArrayPlot[data,FilterRules[{options},Options[ListGridArrayPlot]]],
ListGridGraphicsPlot[data,FilterRules[{options},Options[ListGridGraphicsPlot]]]
]/;If[And[Length[Dimensions[data]]==2,Dimensions[data][[2]]>=2],True,Message[ListGridPlot::input];False];


Options[ListGridPlot]=Select[Options[Graphics]~Join~Options[ArrayPlot],FreeQ[{AspectRatio,ColorFunction,ColorFunctionScaling,Frame,Mesh,MeshStyle,PlotLegends,FrameTicks,FrameLabel},Keys[#]]&]~Join~{
ColorFunction->(Blend[{White,Black},#]&),
ColorFunctionScaling->True,
Mesh->False,
MeshStyle->GrayLevel[GoldenRatio-1],
PlotLegends->Automatic,
Frame->True,
AspectRatio->1,
"NumberFrameTicks"->{5,5},
FrameTicks->Automatic,
FrameLabel->{None,None}
};


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
